Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: python-ipfix
Source: https://github.com/britram/python-ipfix

Files: *
Copyright: 2013-2014 Brian Trammell <brian@trammell.ch>
License: LGPL-3+

Files: ipfix/iana.iespec
       ipfix/rfc5103.iespec
Copyright: 2013-2018 IANA <iana@iana.org>
Comment: These tables of key-value pair are taken from this IANA webpage:
 https://www.iana.org/assignments/ipfix/ipfix.txt
 The following conditions were communicated by email as they are not yet
 published on the website.
License: IANA
 The use of material from the iana.org website is permissible with the
 following conditions:
 .
 1. Provide attribution to the source, including provision of a URL so
    that users can find out the complete context if they choose;
 .
 2. The materials are used in context; You may not edit or selectively
    quote the material to make it false or misleading;
 .
 3. You do not use the materials in a way that implies ICANN sponsorship
    or approval of your work.  This includes not reproducing the ICANN
    or IANA logos separate from where they may appear within the materials.
 .
 With the above conditions, you may use materials from the website.

Files: debian/*
Copyright: 2018 Luca Boccassi <bluca@debian.org>
License: LGPL-3+

License: LGPL-3+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3".
